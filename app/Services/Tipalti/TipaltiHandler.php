<?php

namespace App\Services\Tipalti;

class TipaltiHandler
{
    //Handler for Tipalti API
    public function payerProcessPayments(array $array)
    {
        //Process Tipalti Payment
        return "Payment processed";
    }

    public function payerTestPayments(array $array)
    {
        return "Payment tested";
    }
}
