<?php

namespace App\Models;

use App\Notifications\InvoiceApprovedNotificationForAccounting;
use App\Notifications\InvoiceRejectNotification;
use App\Notifications\PayoutAcceptNotification;
use App\Notifications\PayoutRejectNotification;
use App\Services\Tipalti\TipaltiHandler;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * App\Models\Payout
 *
 * @property int $id
 * @property string $amount
 * @property string $state
 * @property string|null $tipalti_state
 * @property string|null $tipalti_error_messages
 * @property int $refunded
 * @property string|null $rejection_reason
 * @property int $rejection_fee_applied
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Payout newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Payout newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Payout query()
 * @method static \Illuminate\Database\Eloquent\Builder|Payout whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payout whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payout whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payout whereRefunded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payout whereRejectionFeeApplied($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payout whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payout whereTipaltiErrorMessages($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payout whereTipaltiState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payout whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $user_id
 * @method static \Illuminate\Database\Eloquent\Builder|Payout whereTeamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payout whereUserId($value)
 * @property string|null $invoice_path
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Payout whereInvoicePath($value)
 * @property string|null $tipalti_error_message
 * @method static \Illuminate\Database\Eloquent\Builder|Payout whereTipaltiErrorMessage($value)
 * @property string|null $sent_at
 * @method static \Illuminate\Database\Eloquent\Builder|Payout currentMonth()
 * @method static \Illuminate\Database\Eloquent\Builder|Payout whereSendAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payout notRevoked()
 * @method static \Illuminate\Database\Eloquent\Builder|Payout sent()
 * @method static \Illuminate\Database\Eloquent\Builder|Payout whereRejectionReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payout whereSentAt($value)
 * @property string|null $payment_due_date
 * @property string|null $reduced_payment_due_date_at
 * @property string|null $reduced_payment_due_date_fee
 * @property string|null $reduced_payment_due_date_original_amount
 * @property string|null $reduced_payment_due_date_original_due_date
 * @property string $payout_state
 * @property string $invoice_state
 * @method static \Illuminate\Database\Eloquent\Builder|Payout wherePaymentDueDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payout whereReducedPaymentDueDateAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payout whereReducedPaymentDueDateFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payout whereReducedPaymentDueDateOriginalAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payout whereReducedPaymentDueDateOriginalDueDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Payout notCancelled()
 * @method static \Database\Factories\PayoutFactory factory(...$parameters)
 * @property-read int|null $team_amount_snapshots_count
 */

class Payout extends Model
{
    use HasFactory;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function setSuccessOnPendingAndDuePayouts()
    {
        $payouts = Payout::whereRaw("payment_due_date <= NOW()")->where(
            "payout_state",
            "PENDING"
        )->where("invoice_state", "APPROVED")->get();
        foreach ($payouts as $payout) {
            $payout->payout_state = "SUBMITTED";
            $payout->save();
        }
        return $payouts;
    }

    public static function booted()
    {
        self::updating(function (Payout $payout) {
            if ($payout->isDirty('payout_state')) {
                if ($payout->payout_state === 'SUBMITTED') {
                    $payout->user->notify(new PayoutAcceptNotification($payout));
                }
                if ($payout->payout_state === 'REVOKED') {
                    $payout->user->notify(new PayoutRejectNotification($payout));
                }
            }
            if ($payout->isDirty('invoice_state')) {
                if ($payout->invoice_state === 'APPROVED') {
                    // Invoice was accepted
                    $payout->sendInvoiceToAccounting();
                }
                if ($payout->invoice_state === 'DECLINED') {
                    $payout->user->notify(new InvoiceRejectNotification($payout));
                }
            }
        });
    }

    public static function tipaltiSync()
    {
        $finalStates = ["REJECTED", "DEFERRED", "DEFERREDINTERNAL", "CANCELED", "PAID", "CLEARED"];
        /** @var \App\Models\Payout[] $payouts */
        $payouts = self::where("payout_state", "SUBMITTED")->where(function ($q) use ($finalStates) {
            $q->whereNotIn("tipalti_state", $finalStates)
                ->orWhereNull("tipalti_state");
        })->get();
        foreach ($payouts as $payout) {
            echo "{$payout->id}";
            $state = $payout->tipaltiUpdate();
            echo " {$state}" . PHP_EOL;
        }
    }

    public function sendInvoiceToAccounting()
    {
        $emails = ["c.winter@linkvertise.com", "datev@linkvertise.com"];
        foreach ($emails as $email) {
            \Notification::route("mail", $email)
                ->notify(new InvoiceApprovedNotificationForAccounting($this));
        }
    }

    public function invoiceAttachmentName()
    {
        return "Linkvertise Partner Invoice " . $this->id . ".pdf";
    }

    public function tipaltiProcess()
    {
        /** @var TipaltiHandler $tipalti */
        $tipalti = app(TipaltiHandler::class);

        $this->sent_at = now();
        $this->save();


        return $tipalti->payerProcessPayments([
            "idap" => "LV-P-" . $this->team_id,
            "Amount" => $this->amount,
            "RefCode" => "LV-P-" . $this->id,
        ]);
    }

    /**
     * @return mixed
     */
    public function tipaltiPODetail()
    {
        return app(TipaltiHandler::class)->payeePODetails("LV-P-" . $this->id);
    }

    public function tipaltiUpdate()
    {
        if ($this->payout_state != "SUBMITTED") {
            // Payment is not submitted
            return "not ready";
        }

        if ($this->tipalti_state == "REJECTED"
            || $this->tipalti_state == "DEFERRED"
            || $this->tipalti_state == "DEFERREDINTERNAL"
            || $this->tipalti_state == "CANCELED"
            || $this->tipalti_state == "PAID"
            || $this->tipalti_state == "CLEARED") {
            // Payment already has final state
            return $this->tipalti_state;
        }

        $details = $this->tipaltiPODetail();
        if ($details->errorCode == "ParameterError") {
            return;
        }

        $status = $details->POStatus;


        if ($status == "Paid" || $status == "Cleared") {
            $this->tipalti_state = "PAID";
            $this->payout_state = "COMPLETED";
        } elseif ($status == "Rejected" || $status == "Deferred" || $status == "DeferredInternal") {
            $this->payout_state = "REVOKED";
            $this->tipalti_state = Str::upper($status);
            $errorMessage = $details->ErrorMessages->string;
            if (is_array($errorMessage)) {
                $errorMessage = $errorMessage[0];
            }
            $this->tipalti_error_message = $errorMessage;
            // Refund amount because payment was rejected
            $this->refund(true);
        } elseif ($status == "Canceled") {
            $this->payout_state = "REVOKED";
            $this->tipalti_state = Str::upper($status);
            $this->tipalti_error_message = "Payment cancelled due to internal reasons";
            // Refund amount because payment was rejected
            $this->refund(false);
        } else {
            $this->tipalti_state = "PENDING";
        }
        $this->save();
        return $this->tipalti_state;
    }

    public function tipaltiTest()
    {
        /** @var TipaltiHandler $tipalti */
        $tipalti = app(TipaltiHandler::class);

        return $tipalti->payerTestPayments([
            "idap" => "LV-P-" . $this->team_id,
            "Amount" => $this->amount,
            "RefCode" => "LV-P-" . $this->id,
        ]);
    }

    public function getRefundableAmountExcludingRejectionFee()
    {
        return $this->reduced_payment_due_date_original_amount ?? $this->amount;
    }

    public function refund($chargeRejectionFee = false)
    {
        if ($this->refunded) {
            return;
        }

        $this->refunded = 1;
        $this->save();

        $amount = $this->getRefundableAmountExcludingRejectionFee();

        if ($chargeRejectionFee && !$this->rejection_fee_applied) {
            $this->rejection_fee_applied = 1;
            $this->save();
            $amount -= 25;
        }

        $team = $this->team;
        $team->amount += $amount;
        $this->snapshotForPayout($team, true);
        $team->save();
    }

    public function snapshotForPayout($team, $isRefund)
    {
        $insert[] = [
            "team_id" => $team->id,
            "amount" => $isRefund ? $team->amount - $this->amount : $team->amount + (float)$this->amount,
            "payout_id" => $isRefund ? null : $this->id,
            "type" => "PAYOUT",
            "date" => Carbon::today(),
            "created_at" => now(),
        ];

        $insert[] = [
            "team_id" => $team->id,
            "amount" => $team->amount,
            "payout_id" => $isRefund ? $this->id : null,
            "type" => "PAYOUT",
            "date" => Carbon::today(),
            "created_at" => now()->addSeconds(1)
        ];

    }

    public function reverseRefund()
    {
        if ($this->refunded == 0) {
            return;
        }

        $this->refunded = 0;
        $this->save();
        $team = $this->team;
        $team->amount -= $this->amount;
        $team->save();
    }

    public function revoke()
    {
        $this->refund();
        $this->payout_state = "REVOKED";
        $this->save();
    }

    public function reduceDueDate()
    {
        if ($this->invoice_state == "APPROVED" || $this->payout_state != "PENDING") {
            return;
        }

        $currentAmount = $this->amount;
        $remainingDays = now()->diffInDays($this->payment_due_date);

        if ($remainingDays <= 3) {
            return;
        }

        $feePercentage = $this->reducePaymentDueDateFeePercentage($remainingDays);
        $feeAmount = $currentAmount * ($feePercentage);
        $reducedAmount = $this->amount - $feeAmount;

        $this->reduced_payment_due_date_at = now();
        $this->reduced_payment_due_date_fee = $feeAmount;
        $this->reduced_payment_due_date_original_amount = $currentAmount;
        $this->reduced_payment_due_date_original_due_date = $this->payment_due_date;
        $this->payment_due_date = now()->addDays(3);
        $this->amount = $reducedAmount;
        $this->invoice_state = "UPDATE_REQUIRED";
        $this->save();
    }

    public function reducePaymentDueDateFeePercentage($days)
    {

        if ($days > 38) {
            return 0.06;
        } elseif ($days > 31) {
            return 0.05;
        } elseif ($days > 24) {
            return 0.04;
        } elseif ($days > 17) {
            return 0.03;
        } elseif ($days > 10) {
            return 0.02;
        } elseif ($days > 3) {
            return 0.01;
        }
    }
}
